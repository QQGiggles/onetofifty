﻿using Windows.UI.Xaml.Navigation;

namespace OneToFifty
{
    public sealed partial class HighScorePage
    {
        public HighScorePage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if(!string.IsNullOrWhiteSpace((string)e.Parameter))
                listView.Items.Add("Time: " + e.Parameter.ToString());
        }
    }
}
