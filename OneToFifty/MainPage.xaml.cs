﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Media3D;
using Windows.UI.Xaml.Navigation;

namespace OneToFifty
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();

            columnCount = gridMain.ColumnDefinitions.Count;
            rowCount = gridMain.ColumnDefinitions.Count;

            populateGrid();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            dispatcherTimer.Tick += DispatcherTimer_Tick;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            dispatcherTimer.Tick -= DispatcherTimer_Tick;
            dispatcherTimer.Stop();
        }

        private void DispatcherTimer_Tick(object sender, object e)
        {
            timeElapsed = DateTime.Now - start;
            txtBlkTimer.Text = timeElapsed.ToString("m\\:ss");
        }
        
        int columnCount, rowCount;

        DispatcherTimer dispatcherTimer = new DispatcherTimer
        {
            Interval = TimeSpan.FromSeconds(1)
        };

        DateTime start = new DateTime();
        int count = 1;
        TimeSpan timeElapsed;

        static List<int> firstSet = new List<int>()
        {
            1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25
        };
        static List<int> secondSet = new List<int>()
        {
            26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50
        };

        Random _random = new Random();

        /// <summary>
        /// populate grid with initial sequence of numbers
        /// </summary>
        public void populateGrid()
        {
            for (var i = 0; i < columnCount; i++)
                for (var j = 0; j < rowCount; j++)
                {
                    var randomInt = _random.Next(0,firstSet.Count-1);

                    var btn = GameButtonTemplate.LoadContent() as Button;
                    btn.Content = firstSet[randomInt];
                    Grid.SetColumn(btn, i);
                    Grid.SetRow(btn, j+2);
                    gridMain.Children.Add(btn);
                    firstSet.RemoveAt(randomInt);
                }
        }

        /// <summary>
        /// if first buttons are clicked, second set of numbers can be clicked
        /// </summary>
        private async void Button_First_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;

            //start clock when correct
            if (b.Content.Equals(1))
            {
                dispatcherTimer.Start();
                start = DateTime.Now;
            }

            if (b.Content.Equals(count))
            {
                if (secondSet.Count != 0)
                {
                    //if button clicked, change content of button clicked to a random number from the second set of numbers
                    var randomInt = _random.Next(0, secondSet.Count - 1);
                    var value = secondSet[randomInt];
                    secondSet.RemoveAt(randomInt);
                    count++;

                    //remove event so it doesn't happen anymore
                    b.Click -= Button_First_Click;

                    //attach second click event to check new numbers;
                    b.Click += Button_Second_Click;

                    var storyboard = b.Resources["Flip"] as Storyboard;
                    storyboard?.Begin();

                    //for a smooth transition, we wait for the storyboard to be in the middle of its spinning shit,
                    //then we change the content because the button's face isn't visible at 90 degrees
                    var midFlipDelay = TimeSpan.FromTicks(storyboard.Duration.TimeSpan.Ticks / 2);
                    await Task.Delay(midFlipDelay);

                    b.Content = value;
                }
            }
        }

        private void GameButton_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateCenter(sender);
        }

        private void GameButton_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateCenter(sender);
        }

        /// <summary>
        /// 3d transform uses pixel based center so gotta do it in code and do it when the size changes
        /// </summary>
        void UpdateCenter(object sender)
        {
            var b = sender as Button;
            if (b == null) return;

            var transform = b.Transform3D as CompositeTransform3D;
            transform.CenterX = b.ActualWidth / 2;
            transform.CenterY = b.ActualHeight / 2;
        }


        /// <summary>
        /// disable buttons
        /// </summary>
        private void Button_Second_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            if (b.Content.Equals(count))
            {
                //end of game, navigate to welcome screen
                if (b.Content.Equals(50))
                {
                    dispatcherTimer.Stop();

                    //give player option to replay or enter name for high score
                    //replay restarts game, enter name navigates to high score
                    Frame.Navigate(typeof(HighScorePage), timeElapsed);
                }

                //make it look pretty and disappear
                b.Content = "";
                b.IsEnabled = false;

                count++;
            }
            
        }
    }
}
